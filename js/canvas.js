window.Canvas = {};

Canvas.init = function() {
    this.canvas = document.getElementById('canvas');
    this.ctx = this.canvas.getContext('2d');
    this.hundredPercentCanvasWidth = this.canvas.width;
    this.hundredPercentCanavsHeight = this.canvas.height;
    this.width = this.hundredPercentCanvasWidth;
    this.height = this.hundredPercentCanavsHeight;
    this.defaultRectangleWidth = 50;
    this.defaultRectangeHeight = 50;
    this.rectangleObj = [];
    this.textsObjs = [];
    this.zoomingStep = 0.1;

    this.createRnadomRectangleObjects(20);
    this.createRnadomTextsObjects(5);
    this.updateCanvasContent();

    document.getElementById('zoom_in').onclick = function() {
        if(Canvas.width < 2 * Canvas.hundredPercentCanvasWidth) {
            Canvas.zoomIn();
        }
    };
    document.getElementById('zoom_out').onclick = function() {
        if(Canvas.width > 0.6 * Canvas.hundredPercentCanvasWidth) {
            Canvas.zoomOut();
        }
    };
};

Canvas.createRnadomRectangleObjects = function(numberObjsToCreate) {
    for(var i = 0; i < numberObjsToCreate; i++) {
        var originX = this.width * Math.random();
        var originY = this.height * Math.random();
        this.rectangleObj[i] = {
            'originX': originX,
            'x': originX,
            'originY': originY,
            'y': originY,
            'originWidth': this.defaultRectangleWidth,
            'width': this.defaultRectangleWidth,
            'originHeight': this.defaultRectangeHeight,
            'height': this.defaultRectangeHeight
        };
    };
};

Canvas.createRnadomTextsObjects = function(numberObjsToCreate) {
    for(var i = 0; i < numberObjsToCreate; i++) {
        var originX = this.width * Math.random();
        var originY = this.height * Math.random();
        this.textsObjs[i] = {
            'originX': originX,
            'x': originX,
            'originY': originY,
            'y': originY,
            'text': 'hello from Cowboy Bebop',
            'originFontHeight': 30,
            'fontHeight': 30
        };
    };
}

Canvas.updateCanvasContent = function() {
    this.ctx.clearRect(0, 0, this.width, this.height);
    this.rectangleObj.forEach(function(obj) {
        Canvas.ctx.fillRect(obj.x, obj.y, obj.width, obj.height);
    });
    this.textsObjs.forEach(function(text){
        Canvas.ctx.save();
        Canvas.ctx.font = text.fontHeight + 'px Arial';
        Canvas.ctx.fillText(text.text, text.x, text.y);
        Canvas.ctx.restore();
    });
};

Canvas.zoomIn = function() {
    this.width = this.getScalebleDimension(
        this.hundredPercentCanvasWidth,
        this.width,
        true
    );
    this.height = this.getScalebleDimension(
        this.hundredPercentCanavsHeight,
        this.height,
        true
    );
    this.rectangleObj.forEach(function(obj) {
        Canvas.scaleRectObj(obj, true);
    });
    this.textsObjs.forEach(function(obj) {
        Canvas.scaleTextObj(obj, true);
    });
    this.scaleContent();
};

Canvas.zoomOut = function() {
    this.width = this.getScalebleDimension(
        this.hundredPercentCanvasWidth,
        this.width,
        false
    );
    this.height = this.getScalebleDimension(
        this.hundredPercentCanavsHeight,
        this.height,
        false
    );
    this.rectangleObj.forEach(function(obj) {
        Canvas.scaleRectObj(obj, false);
    });
    this.textsObjs.forEach(function(obj) {
        Canvas.scaleTextObj(obj, false);
    });
    this.scaleContent();
};

Canvas.getScalebleDimension = function(oldValue, newValue, isItzoomIn) {
    if(isItzoomIn) {
        var scalebleValue = newValue + this.zoomingStep * oldValue;
    } else {
        var scalebleValue = newValue - this.zoomingStep * oldValue;
    }

    return scalebleValue;
};

Canvas.scaleContent = function() {
    this.canvas.setAttribute('width', this.width);
    this.canvas.setAttribute('height', this.height);
    this.updateCanvasContent();
};

Canvas.scaleRectObj = function(obj, isItZoomInAction) {
    if(isItZoomInAction) {
        obj.x = obj.x + this.zoomingStep * obj.originX;
        obj.y = obj.y + this.zoomingStep * obj.originY;
        obj.width = obj.width + this.zoomingStep * obj.originWidth;
        obj.height = obj.height + this.zoomingStep * obj.originHeight;
    } else {
        obj.x = obj.x - this.zoomingStep * obj.originX;
        obj.y = obj.y - this.zoomingStep * obj.originY;
        obj.width = obj.width - this.zoomingStep * obj.originWidth;
        obj.height = obj.height - this.zoomingStep * obj.originHeight;
    }
};

Canvas.scaleTextObj = function(obj, isItZoomInAction) {
    if(isItZoomInAction) {
        obj.x = obj.x + this.zoomingStep * obj.originX;
        obj.y = obj.y + this.zoomingStep * obj.originY;
        obj.fontHeight = obj.fontHeight + this.zoomingStep * obj.originFontHeight;
    } else {
        obj.x = obj.x - this.zoomingStep * obj.originX;
        obj.y = obj.y - this.zoomingStep * obj.originY;
        obj.fontHeight = obj.fontHeight - this.zoomingStep * obj.originFontHeight;
    }
};